package com.test.model.entity;

import jakarta.persistence.*;

@Entity
@Table(name = "mobil")
public class Mobil {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String warna;
    private Integer kecepatan;
    private Double harga;
    private String merk;

    public Mobil() {
    }

    public Mobil(Long id, String warna, Integer kecepatan, Double harga, String merk) {
        this.id = id;
        this.warna = warna;
        this.kecepatan = kecepatan;
        this.harga = harga;
        this.merk = merk;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getWarna() {
        return warna;
    }

    public void setWarna(String warna) {
        this.warna = warna;
    }

    public Integer getKecepatan() {
        return kecepatan;
    }

    public void setKecepatan(Integer kecepatan) {
        this.kecepatan = kecepatan;
    }

    public Double getHarga() {
        return harga;
    }

    public void setHarga(Double harga) {
        this.harga = harga;
    }

    public String getMerk() {
        return merk;
    }

    public void setMerk(String merk) {
        this.merk = merk;
    }
}
