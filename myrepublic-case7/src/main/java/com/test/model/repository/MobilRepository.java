package com.test.model.repository;

import com.test.model.entity.Mobil;
import io.quarkus.hibernate.orm.panache.PanacheRepository;
import jakarta.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class MobilRepository implements PanacheRepository<Mobil> {
}

