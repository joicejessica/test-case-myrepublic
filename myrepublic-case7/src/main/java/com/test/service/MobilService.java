package com.test.service;

import com.test.model.entity.Mobil;

import java.util.List;

public interface MobilService {

    List<Mobil> findAll();
    Mobil findById(Long id);
    Mobil create(Mobil mobil);
    Mobil update(Long id, Mobil mobil);
    void delete(Long id);
}
