package com.test.service.impl;

import com.test.model.entity.Mobil;
import com.test.model.repository.MobilRepository;
import com.test.service.MobilService;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.transaction.Transactional;

import java.util.List;

@ApplicationScoped
public class MobilServiceImpl implements MobilService {

    private final MobilRepository mobilRepository;

    public MobilServiceImpl(MobilRepository mobilRepository) {
        this.mobilRepository = mobilRepository;
    }

    @Override
    public List<Mobil> findAll() {
        return mobilRepository.listAll();
    }

    @Override
    public Mobil findById(Long id) {
        return mobilRepository.findById(id);
    }

    @Override
    @Transactional
    public Mobil create(Mobil mobil) {
        mobilRepository.persist(mobil);
        return mobil;
    }

    @Override
    @Transactional
    public Mobil update(Long id, Mobil updatedMobil) {
        Mobil mobil = findById(id);
        if (mobil != null) {
            mobil.setWarna(updatedMobil.getWarna());
            mobil.setKecepatan(updatedMobil.getKecepatan());
            mobil.setHarga(updatedMobil.getHarga());
            mobil.setMerk(updatedMobil.getMerk());
        }
        return mobil;
    }

    @Override
    @Transactional
    public void delete(Long id) {
        mobilRepository.deleteById(id);
    }
}
