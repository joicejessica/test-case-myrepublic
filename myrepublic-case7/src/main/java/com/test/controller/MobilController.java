package com.test.controller;

import com.test.model.entity.Mobil;
import com.test.service.MobilService;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;

import java.util.List;

@Path("/mobil")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class MobilController {

    private final MobilService service;

    public MobilController(MobilService service) {
        this.service = service;
    }

    @GET
    @Path("/findAllMobil")
    public List<Mobil> getAllMobil() {
        return service.findAll();
    }

    @GET
    @Path("/findMobilById/{id}")
    public Mobil getMobil(@PathParam("id") Long id) {
        return service.findById(id);
    }

    @POST
    @Path("/addMobil")
    public Mobil createMobil(Mobil mobil) {
        return service.create(mobil);
    }

    @PUT
    @Path("/updateMobil/{id}")
    public Mobil updateMobil(@PathParam("id") Long id, Mobil mobil) {
        return service.update(id, mobil);
    }

    @DELETE
    @Path("/deleteMobil/{id}")
    public void deleteMobil(@PathParam("id") Long id) {
        service.delete(id);
    }
}
