package com.test.myrepublic.case1;

public class Mobil {

    String warna;
    int kecepatan;
    double harga;
    String merk;

    public Mobil(String warna, int kecepatan, double harga, String merk) {
        this.warna = warna;
        this.kecepatan = kecepatan;
        this.harga = harga;
        this.merk = merk;
    }

    public void tampilkanInfo() {
        System.out.println("Warna: " + warna);
        System.out.println("Kecepatan: " + kecepatan + " km/jam");
        System.out.println("Harga: Rp " + harga);
        System.out.println("Merk: " + merk);
    }

    public static void main(String[] args) {
        Mobil[] mobil = new Mobil[3];
        mobil[0] = new Mobil("Merah", 120, 300000000, "Toyota");
        mobil[1] = new Mobil("Biru", 130, 350000000, "Honda");
        mobil[2] = new Mobil("Grey", 110, 250000000, "Suzuki");

        for (int i = 0; i < mobil.length; i++) {
            System.out.println("Informasi Mobil " + (char)('A' + i) + ":");
            mobil[i].tampilkanInfo();
            System.out.println();
        }
    }
}
