package com.test.myrepublic.case5;

public class AbjadAlgoritma {
    public static void main(String[] args) {
        int rowCount = 5;
        char currentChar = 'A';

        for (int i = 1; i <= rowCount; i++) {
            // Membuat leading spaces
            for (int j = 1; j <= rowCount - i; j++) {
                System.out.print("  "); // Menambahkan dua spasi setiap kali
            }

            // Membuat karakter di sisi kiri
            for (int j = 1; j <= i * 2 - 1; j++) {
                System.out.print(currentChar);
                currentChar++;
                if (j < i * 2 - 1) {
                    System.out.print(" "); // Menambahkan satu spasi di antara karakter
                }
            }

            // Pindah ke baris berikutnya
            System.out.println();
        }
    }
}
