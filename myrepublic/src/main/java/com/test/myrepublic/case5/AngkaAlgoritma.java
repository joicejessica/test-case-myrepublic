package com.test.myrepublic.case5;

public class AngkaAlgoritma {
    public static void main(String[] args) {
        int n = 7; // Ganti n sesuai dengan tinggi segitiga yang diinginkan

        // Menghitung koefisien binomial dan mencetak segitiga
        for (int i = 0; i < n; i++) {
            int number = 1;
            for (int j = 0; j < n - i; j++) {
                System.out.print(" ");
            }
            for (int j = 0; j <= i; j++) {
                System.out.print(" " + number);
                number = number * (i - j) / (j + 1);
            }
            System.out.println();
        }
    }
}
