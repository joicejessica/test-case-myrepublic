package com.test.myrepublic.case2;

public class MacKonversi {
    public static void main(String[] args) {
        String input = "21090022002346,FCD5D9D34A6F 21090022002838,FCD5D9D34C5B 21090022002965,FCD5D9D34CDA 21090022003748,FCD5D9D34FE9 21090022003798,FCD5D9D3501B";

        String[] lines = input.split(" ");

        StringBuilder output = new StringBuilder();

        for (String line : lines) {
            String[] parts = line.split(",");

            if (parts.length == 2) {
                String mac = parts[1];
                String convertedMac = convertMacFormat(mac);
                String resultLine = parts[0] + "," + convertedMac;

                output.append(resultLine).append("\n");
            }
        }

        System.out.println(output.toString().trim());
    }

    private static String convertMacFormat(String mac) {
        StringBuilder convertedMac = new StringBuilder();

        for (int i = 0; i < mac.length(); i += 2) {
            convertedMac.append(mac.substring(i, i + 2));

            if (i < mac.length() - 2) {
                convertedMac.append(":");
            }
        }

        return convertedMac.toString();
    }
}
