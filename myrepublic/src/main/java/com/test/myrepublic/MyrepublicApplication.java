package com.test.myrepublic;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MyrepublicApplication {

	public static void main(String[] args) {
		SpringApplication.run(MyrepublicApplication.class, args);
	}

}
