package com.test.myrepublic.case6.controller;

import com.test.myrepublic.case6.dto.UserDTO;
import com.test.myrepublic.case6.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping("/user/{userId}")
    public ResponseEntity<String> getUserInfo(@PathVariable int userId) {
        UserDTO userDTO = userService.getUserInfo(userId);
        String response = "<b> Name: </b> " + userDTO.getName() + "<br>" +
                "<b> Website: </b> " + userDTO.getWebsite() + "<br>" +
                "<b> Email: </b>" + userDTO.getEmail() + "<br>" +
                "<b> Phone: </b>" + userDTO.getPhone();

        return ResponseEntity.ok(response);
    }
}
