package com.test.myrepublic.case6.dto;

import lombok.Data;

@Data
public class UserDTO {

    private String name;

    private String website;

    private String email;

    private String phone;

}
