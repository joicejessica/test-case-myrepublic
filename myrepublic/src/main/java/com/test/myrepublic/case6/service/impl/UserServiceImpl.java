package com.test.myrepublic.case6.service.impl;

import com.test.myrepublic.case6.dto.UserDTO;
import com.test.myrepublic.case6.service.UserService;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Map;

@Service
public class UserServiceImpl implements UserService {

    @Override
    public UserDTO getUserInfo(int userId) {
        RestTemplate restTemplate = new RestTemplate();
        String apiUrl = "https://jsonplaceholder.typicode.com/users/" + userId;
        ResponseEntity<Map> response = restTemplate.getForEntity(apiUrl, Map.class);

        Map<String, Object> userData = response.getBody();
        UserDTO userDTO = new UserDTO();
        userDTO.setName(userData.get("name").toString());
        userDTO.setWebsite(userData.get("website").toString());
        userDTO.setEmail(userData.get("email").toString());
        userDTO.setPhone(userData.get("phone").toString());

        return userDTO;
    }
}
