package com.test.myrepublic.case6.service;

import com.test.myrepublic.case6.dto.UserDTO;

public interface UserService {

    UserDTO getUserInfo(int userId);
}
