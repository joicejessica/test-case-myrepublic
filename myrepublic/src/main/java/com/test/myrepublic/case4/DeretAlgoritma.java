package com.test.myrepublic.case4;

public class DeretAlgoritma {
    public static void main(String[] args) {
        cetakDeret("a.", 5, 3, 10);
        cetakDeret("b.", 5, 3, 10);
        cetakDeret("c.", 1, 2, 10);
    }

    public static void cetakDeret(String label, int awal, int selisih, int n) {
        System.out.print(label + " ");
        for (int i = 0; i < n; i++) {
            System.out.print(awal + " ");
            awal += selisih;
            selisih += (label.equals("c.") ? 2 : 0);
        }
        System.out.println();
    }
}
